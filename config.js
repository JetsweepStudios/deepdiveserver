const Joi = require('joi')

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config()

// define validation for all the env vars
const envVarsSchema = Joi.object({
	NODE_ENV: Joi.string().allow(['development', 'test', 'production']),
	PORT: Joi.number().default(3000),
	MONGO_URI: Joi.string()
		.required()
		.description('Mongo DB dev or prod host URI'),
	MONGO_URI_TEST: Joi.string()
		.required()
		.description('Mongo DB test URI')
})
	.unknown() //allow unknown keys
	.required()

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema)
if (error) {
	throw new Error(`Config validation error: ${error.message}`)
}


module.exports = {
	env: envVars.NODE_ENV,
	port: envVars.PORT,
	mongoUri:
		envVars.NODE_ENV === 'test' ? envVars.MONGO_URI_TEST : envVars.MONGO_URI,
	githubClientID: envVars.GITHUB_CLIENT_ID,
	githubClientSecret: envVars.GITHUB_CLIENT_SECRET,
	jwtSecret: envVars.JWT_SECRET
}
