const mongoose = require('mongoose')

var parser = {


	checkFA(data) {
		data.map(player => {

			if (player.experiencePoints > 10000 && player.age < 28) {

				console.log(`${player.position}: ${player.firstName} ${player.lastName} - ${player.experiencePoints}`)
			}

		})

	},

	addSkillPlayers(players) {
		const parsedPlayers = []

		players.forEach(player => {

			// IGNORE PRACTICE SQUAD PLAYERS
			if (player.isOnPracticeSquad) {
				return;
			}

			const newPlayer = {
				_id: player.rosterId,
				playerId: player.rosterId,
				firstName: player.firstName,
				lastName: player.lastName,
				position: player.position,
				teamId: player.teamId,
				age: player.age,
				speedRating: player.speedRating,
				agilityRating: player.agilityRating,
				accelRating: player.accelRating,
				awareRating: player.awareRating,
				strengthRating: player.strengthRating,


			}

			switch (player.position) {

				case 'QB':
					newPlayer.throwAccShortRating = player.throwAccShortRating
					newPlayer.throwAccMidRating = player.throwAccMidRating
					newPlayer.throwAccDeepRating = player.throwAccDeepRating
					newPlayer.throwPowerRating = player.throwPowerRating
					newPlayer.throwOnRunRating = player.throwOnRunRating
					newPlayer.playActionRating = player.playActionRating
					parsedPlayers.push(newPlayer)

					break;

				case 'HB':
					newPlayer.carryRating = player.carryRating
					newPlayer.catchRating = player.catchRating
					newPlayer.bCVRating = player.bCVRating
					newPlayer.elusiveRating = player.elusiveRating
					newPlayer.routeRunRating = player.routeRunRating
					newPlayer.truckRating = player.truckRating
					newPlayer.stiffArmRating = player.stiffArmRating
					newPlayer.spinMoveRating = player.spinMoveRating
					newPlayer.jukeMoveRating = player.jukeMoveRating
					parsedPlayers.push(newPlayer)

					break;

				case 'WR':
					newPlayer.carryRating = player.carryRating
					newPlayer.catchRating = player.catchRating
					newPlayer.bCVRating = player.bCVRating
					newPlayer.elusiveRating = player.elusiveRating
					newPlayer.routeRunRating = player.routeRunRating
					newPlayer.truckRating = player.truckRating
					newPlayer.stiffArmRating = player.stiffArmRating
					newPlayer.spinMoveRating = player.spinMoveRating
					newPlayer.jukeMoveRating = player.jukeMoveRating
					newPlayer.cITRating = player.cITRating
					newPlayer.releaseRating = player.releaseRating
					newPlayer.specCatchRating = player.specCatchRating
					newPlayer.jumpRating = player.jumpRating
					parsedPlayers.push(newPlayer)

					break;

				case 'TE':
					newPlayer.carryRating = player.carryRating
					newPlayer.catchRating = player.catchRating
					newPlayer.bCVRating = player.bCVRating
					newPlayer.elusiveRating = player.elusiveRating
					newPlayer.routeRunRating = player.routeRunRating
					newPlayer.truckRating = player.truckRating
					newPlayer.stiffArmRating = player.stiffArmRating
					newPlayer.spinMoveRating = player.spinMoveRating
					newPlayer.jukeMoveRating = player.jukeMoveRating
					newPlayer.cITRating = player.cITRating
					newPlayer.releaseRating = player.releaseRating
					newPlayer.specCatchRating = player.specCatchRating
					newPlayer.jumpRating = player.jumpRating
					newPlayer.passBlockRating = player.passBlockRating
					newPlayer.impactBlockRating = player.impactBlockRating
					newPlayer.runBlockRating = player.runBlockRating
					parsedPlayers.push(newPlayer)

					break;

					case 'FB':
					newPlayer.carryRating = player.carryRating
					newPlayer.catchRating = player.catchRating
					newPlayer.bCVRating = player.bCVRating
					newPlayer.elusiveRating = player.elusiveRating
					newPlayer.truckRating = player.truckRating
					newPlayer.stiffArmRating = player.stiffArmRating
					newPlayer.passBlockRating = player.passBlockRating
					newPlayer.impactBlockRating = player.impactBlockRating
					newPlayer.runBlockRating = player.runBlockRating
					parsedPlayers.push(newPlayer)

					break;

				default:
					return;
					break;


			}
		})



		return parsedPlayers
	},



}

module.exports = parser
