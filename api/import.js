const express = require("express");
const router = express.Router();

const League = require("../models/leagueModel");
const PassingData = require("../models/statModels/passingDataModel");
const RushingData = require("../models/statModels/rushingDataModel");
const ReceivingData = require("../models/statModels/receivingDataModel");
const DefenseData = require("../models/statModels/defenseDataModel");
const Teams = require("../models/teamModel");
const ScoringData = require("../models/scoringDataModel");
const Roster = require("../models/rosterModel");

var teamDataParser = require("./../src/middleware/teamDataParser.js");
var scoringDataParser = require("./../src/middleware/scoringDataParser.js");
var rosterParser = require("./../src/middleware/rosterParser.js");

router.get("/", async function(req, res) {
  console.log("test");
  res.json({ result: true });
});

router.post("/:abv/*", async function(req, res) {
  const abv = req.params.abv;
  const leagueId = req.params[0].split("/")[1];
  const consoleType = req.params[0].split("/")[0];

  console.log("receiving post for league: " + abv + " - " + leagueId);

  if (!abv) {
    res.end();
  }
  try {
    const foundLeague = await League.findOne({
      $or: [{ abbreviation: abv }, { leagueId: leagueId }]
    });

    // if league doesn't exist, do nothing
    if (!foundLeague) {
      res.status(404).send("league not found");

      res.end();
    }

    // if league is found, but isn't active this is the first time data is being sent
    // set league status to active and set up
    if (!foundLeague.leagueActivated) {
      console.log("activating league: " + abv);
      foundLeague.leagueActivated = true;
      foundLeague.leagueId = leagueId;
      foundLeague.console = consoleType;
      const teams = await new Teams({ leagueId: leagueId });
      const scoring = await new ScoringData({ leagueId: leagueId });
      const passing = await new PassingData({ leagueId: leagueId });
      const rushing = await new RushingData({ leagueId: leagueId });
      const receiving = await new ReceivingData({ leagueId: leagueId });
      const defense = await new DefenseData({ leagueId: leagueId });

      foundLeague.teams = teams._id;
      foundLeague.scoring = scoring._id;
      foundLeague.passing = passing._id;
      foundLeague.rushing = rushing._id;
      foundLeague.receiving = receiving._id;
      foundLeague.defense = defense._id;
      await teams.save();
      await scoring.save();
      await passing.save();
      await rushing.save();
      await receiving.save();
      await defense.save();
      await foundLeague.save();
    }

    // IF THE LEAGUE WAS FOUND, BUT THE ABBREVIATION OF LEAGUE ID DOESN'T MATCH
    // THEN THE USER IS SENDING INFO FOR THE WRONG LEAGUE, SO END THE CONNECTION
    if (foundLeague.abbreviation !== abv || foundLeague.leagueId !== leagueId) {
      res.status(403).send("leagueID and abbreviation don't match");
      res.end();
    }

    // parse incoming data
    const importType = req.params[0].split("/");

    if (importType.includes("leagueteams")) {
      const teamData = await Teams.findOne({ _id: foundLeague.teams });

      if (teamData.teams.length === 0) {
        teamData.teams = teamDataParser.addTeamData(req.body.leagueTeamInfoList);
      } else {
        teamData.teams = teamDataParser.updateExistingTeamData(teamData.teams, req.body.leagueTeamInfoList);
      }
      await teamData.save();
    }

    if (importType.includes("standings")) {
      const teamData = await Teams.findOne({ _id: foundLeague.teams });
      const { weekIndex, seasonIndex, stageIndex } = req.body.teamStandingInfoList[0];

      foundLeague.currentSeason = seasonIndex;
      foundLeague.currentWeek = weekIndex;
      foundLeague.currentStage = stageIndex;
      // NEED TO CHECK STAGE INDEX, THINK 0 IS PRESEASON, 1 IS REGULAR SEASON AND 2 IS OFFSEASON
      if ((teamData.teams && stageIndex === 1) || stageIndex === 2) {
        teamData.teams = teamDataParser.updateUsers(teamData.teams, weekIndex, seasonIndex);
        await teamData.save();
      }
      await foundLeague.save();
    }

    // ONLY CHECKING TEAMS ON ACTIVE ROSTER NOW, FREE AGENTS WHEN ON ACTIVE ROSTER WILL BE ADDED THEN AND REMAIN EVEN AFTER
    if (importType.includes("team")) {
      // if roster hasn't been created yet
      let roster;
      if (!foundLeague.roster) {
        roster = await new Roster({ leagueId: leagueId });
        foundLeague.roster = roster._id;
        await foundLeague.save();
        await roster.save();
      } else {
        roster = await Roster.findOne({ _id: foundLeague.roster });
      }

      const newPlayers = await rosterParser.addSkillPlayers(req.body.rosterInfoList);

      newPlayers.forEach(player => {
        // if the player already is in DB, remove them
        try {
          if (roster.skillPositions.id(player._id)) {
            roster.skillPositions.id(player._id).remove();
          }
          // add the player to the DB
          roster.skillPositions.push(player);
        } catch (e) {
          console.log(e);
        }
      });

      await roster.save();
      res.end();
    }
    if (importType.includes("[pre]")) {
      res.end();
    }

    if (importType.includes("reg")) {
      const week = req.params[0].split("/")[4] - 1; // -1 to get weeks to 0 index
      const dataType = req.params[0].split("/")[5];
      const scoring = await ScoringData.findOne({ _id: foundLeague.scoring });
      let season;
      let weekCompleted;
      switch (dataType) {
        case "schedules":
          season = req.body.gameScheduleInfoList[0].seasonIndex;
          try {
            if (scoring.data[season][week].weekCompleted) {
              break;
            }
          } catch (e) {}

          let [parsedData, weekCompleted] = scoringDataParser.addScoringData(req.body.gameScheduleInfoList, week);
          if (scoring.data[season]) {
            if (scoring.data[season][week]) {
              scoring.data[season][week] = {
                week: week,
                season: season,
                teamData: parsedData,
                weekCompleted: weekCompleted
              };
            } else {
              scoring.data[season][week] = {
                week: week,
                season: season,
                teamData: parsedData,
                weekCompleted: weekCompleted
              };
            }
          } else {
            scoring.data[season] = [];
            scoring.data[season][week] = {
              week: week,
              season: season,
              teamData: parsedData,
              weekCompleted: weekCompleted
            };
          }

          await scoring.save();

          break;
        case "passing":
          const DB_PASSING_DATA = await PassingData.findOne({ _id: foundLeague.passing });

          season = req.body.playerPassingStatInfoList[0].seasonIndex;
          let passWeekCompleted = false;
          try {
            passWeekCompleted = DB_PASSING_DATA.passingData[season][week].weekCompleted;
          } catch (e) {}

          try {
            if (scoring.data[season][week].weekCompleted && passWeekCompleted) {
              console.log(`Season ${season}, Week ${week} passing data locked, skipping import`);

              break;
            }
          } catch (e) {}

          let passingData = scoringDataParser.addPassingData(req.body.playerPassingStatInfoList, week);
          if (DB_PASSING_DATA.passingData[season]) {
            if (DB_PASSING_DATA.passingData[season][week]) {
              DB_PASSING_DATA.passingData[season][week] = {
                week: week,
                season: season,
                playerStats: passingData
              };
            } else {
              DB_PASSING_DATA.passingData[season][week] = {
                week: week,
                season: season,
                playerStats: passingData
              };
            }
          } else {
            DB_PASSING_DATA.passingData[season] = [];
            DB_PASSING_DATA.passingData[season][week] = {
              week: week,
              season: season,
              playerStats: passingData
            };
          }

          try {
            if (scoring.data[season][week].weekCompleted) {
              DB_PASSING_DATA.passingData[season][week].weekCompleted = true;
            }
          } catch (e) {}

          await DB_PASSING_DATA.save();
          break;

        case "rushing":
          const DB_RUSHING_DATA = await RushingData.findOne({ _id: foundLeague.rushing });

          season = req.body.playerRushingStatInfoList[0].seasonIndex;
          let rushWeekCompleted = false;
          try {
            rushWeekCompleted = DB_RUSHING_DATA.rushingData[season][week].weekCompleted;
          } catch (e) {}

          try {
            if (scoring.data[season][week].weekCompleted && rushWeekCompleted) {
              console.log(`Season ${season}, Week ${week} rushing data locked, skipping import`);

              break;
            }
          } catch (e) {}

          let rushingData = scoringDataParser.addRushingData(req.body.playerRushingStatInfoList, week);
          if (DB_RUSHING_DATA.rushingData[season]) {
            if (DB_RUSHING_DATA.rushingData[season][week]) {
              DB_RUSHING_DATA.rushingData[season][week] = {
                week: week,
                season: season,
                playerStats: rushingData
              };
            } else {
              DB_RUSHING_DATA.rushingData[season][week] = {
                week: week,
                season: season,
                playerStats: rushingData
              };
            }
          } else {
            DB_RUSHING_DATA.rushingData[season] = [];
            DB_RUSHING_DATA.rushingData[season][week] = {
              week: week,
              season: season,
              playerStats: rushingData
            };
          }

          try {
            if (scoring.data[season][week].weekCompleted) {
              DB_RUSHING_DATA.rushingData[season][week].weekCompleted = true;
            }
          } catch (e) {}

          await DB_RUSHING_DATA.save();
          break;

        case "receiving":
          const DB_RECEIVING_DATA = await ReceivingData.findOne({ _id: foundLeague.receiving });

          season = req.body.playerReceivingStatInfoList[0].seasonIndex;
          let recWeekCompleted = false;
          try {
            recWeekCompleted = DB_RECEIVING_DATA.receivingData[season][week].weekCompleted;
          } catch (e) {}

          try {
            if (scoring.data[season][week].weekCompleted && recWeekCompleted) {
              console.log(`Season ${season}, Week ${week} receiving data locked, skipping import`);

              break;
            }
          } catch (e) {}

          let receivingData = scoringDataParser.addReceivingData(req.body.playerReceivingStatInfoList, week);
          if (DB_RECEIVING_DATA.receivingData[season]) {
            if (DB_RECEIVING_DATA.receivingData[season][week]) {
              DB_RECEIVING_DATA.receivingData[season][week] = {
                week: week,
                season: season,
                playerStats: receivingData
              };
            } else {
              DB_RECEIVING_DATA.receivingData[season][week] = {
                week: week,
                season: season,
                playerStats: receivingData
              };
            }
          } else {
            DB_RECEIVING_DATA.receivingData[season] = [];
            DB_RECEIVING_DATA.receivingData[season][week] = {
              week: week,
              season: season,
              playerStats: receivingData
            };
          }
          try {
            if (scoring.data[season][week].weekCompleted) {
              DB_RECEIVING_DATA.receivingData[season][week].weekCompleted = true;
            }
          } catch (e) {}

          await DB_RECEIVING_DATA.save();
          break;

        case "defense":
          const DB_DEFENSE_DATA = await DefenseData.findOne({ _id: foundLeague.defense });
          season = req.body.playerDefensiveStatInfoList[0].seasonIndex;

          let defWeekCompleted = false;
          try {
            defWeekCompleted = DB_DEFENSE_DATA.defenseData[season][week].weekCompleted;
          } catch (e) {}

          try {
            if (scoring.data[season][week].weekCompleted && defWeekCompleted) {
              console.log(`Season ${season}, Week ${week} defensive data locked, skipping import`);
              break;
            }
          } catch (e) {}

          let defenseData = scoringDataParser.addDefensiveData(req.body.playerDefensiveStatInfoList, week);
          if (DB_DEFENSE_DATA.defenseData[season]) {
            if (DB_DEFENSE_DATA.defenseData[season][week]) {
              DB_DEFENSE_DATA.defenseData[season][week] = {
                week: week,
                season: season,
                playerStats: defenseData
              };
            } else {
              DB_DEFENSE_DATA.defenseData[season][week] = {
                week: week,
                season: season,
                playerStats: defenseData
              };
            }
          } else {
            DB_DEFENSE_DATA.defenseData[season] = [];
            DB_DEFENSE_DATA.defenseData[season][week] = {
              week: week,
              season: season,
              playerStats: defenseData
            };
          }

          try {
            if (scoring.data[season][week].weekCompleted) {
              DB_DEFENSE_DATA.defenseData[season][week].weekCompleted = true;
            }
          } catch (e) {}

          await DB_DEFENSE_DATA.save();
          break;

        default:
          res.end();
          break;
      }
    }
  } catch (e) {
    console.log(e);
  }

  res.end();
});

module.exports = router;
