const mongoose = require('mongoose')
const Schema = mongoose.Schema

const scoringDataSchema = new Schema(
	{
		leagueId: String,
		data: [[{
			season: Number,
			week: Number,
			weekCompleted: Boolean,
			teamData: [{
				teamID: String,
				opponentTeamID: String,
				pointsScored: Number,
				pointsAllowed: Number,
				status: Number,
				_id: false

			}
			]
		}]]
	}

)

const scoringDataModel = mongoose.model('scoringData', scoringDataSchema, 'scoringData')
module.exports = scoringDataModel
