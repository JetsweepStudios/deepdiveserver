const mongoose = require('mongoose')
const Schema = mongoose.Schema

const receivingDataSchema = new Schema(
	{
		week: Number,
		season: Number,
		weekCompleted: Boolean,
		playerStats: [{
			teamId: String,
			rosterId: String,
			recCatches: Number,
			recDrops: Number,
			recLongest: Number,
			recPts: Number, // VERIFY IF THIS IS A 2PC
			recYds: Number,
			recTDs: Number,
			recYdsAfterCatch: Number,
			_id: false


		}
		]
	}, {
		_id: false
	}

)

module.exports = receivingDataSchema
