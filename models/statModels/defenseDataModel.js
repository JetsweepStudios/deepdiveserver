const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DefenseData = require('./defenseDataSchema')


const defenseDataSchema = new Schema(
  {
    leagueId: String,
    //SCORING DATA TRACKED ACROSS HISTORY OF LEAGUE, SO IN NESTED ARRAY
    // FIRST FOR SEASON, SECOND FOR WEEK
    defenseData: [[DefenseData]],

  }, {
    timestamps: true
  }
);

const defenseDataModel = mongoose.model('defenseData', defenseDataSchema, 'defenseData');
module.exports = defenseDataModel;