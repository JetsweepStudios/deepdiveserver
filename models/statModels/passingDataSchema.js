const mongoose = require('mongoose')
const Schema = mongoose.Schema

const passingDataSchema = new Schema(
	{
		week: Number,
		season: Number,
		weekCompleted: Boolean,
		playerStats: [{
			teamId: String,
			rosterId: String,
			passAtt: Number,
			passComp: Number,
			passInts: Number,
			passLongest: Number,
			passPts: Number, // VERIFY IF THIS IS A 2PC
			passYds: Number,
			passTDs: Number,
			passSacks: Number,
			_id: false

		}
		]
	}, {
		_id: false
	}

)

module.exports = passingDataSchema
