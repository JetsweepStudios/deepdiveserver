const mongoose = require('mongoose')
const Schema = mongoose.Schema

const rushingDataSchema = new Schema(
	{
		week: Number,
		season: Number,
		weekCompleted: Boolean,
		playerStats: [{
			teamId: String,
			rosterId: String,
			rushAtt: Number,
			rushBrokenTackles: Number,
			rushFum: Number,
			rushLongest: Number,
			rushPts: Number, // VERIFY IF THIS IS A 2PC
			rushYds: Number,
			rushTDs: Number,
			rush20PlusYds: Number,
			rushYdsAfterContact: Number,
			_id: false

		}
		]
	}, {
		_id: false
	}

)

module.exports = rushingDataSchema
