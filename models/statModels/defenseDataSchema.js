const mongoose = require('mongoose')
const Schema = mongoose.Schema

const defensiveDataSchema = new Schema(
	{
		week: Number,
		season: Number,
		weekCompleted: Boolean,
		playerStats: [{
			teamId: String,
			rosterId: String,
			defCatchAllowed: Number,
			defDeflections: Number,
			defForcedFum: Number,
			defFumRec: Number,
			defInts: Number,
			defIntReturnYds: Number,
			defSacks: Number,
			defSafeties: Number,
			defTDs: Number,
			defTotalTackles: Number,
			_id: false

		}
		]
	}, {
		_id: false
	}

)

module.exports = defensiveDataSchema
