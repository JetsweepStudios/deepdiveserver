const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PassingData = require('./passingDataSchema')


const passingDataSchema = new Schema(
  {
    leagueId: String,
    //SCORING DATA TRACKED ACROSS HISTORY OF LEAGUE, SO IN NESTED ARRAY
    // FIRST FOR SEASON, SECOND FOR WEEK
    passingData: [[PassingData]],

  }, {
    timestamps: true
  }
);

const passingDataModel = mongoose.model('passingData', passingDataSchema, 'passingData');
module.exports = passingDataModel;