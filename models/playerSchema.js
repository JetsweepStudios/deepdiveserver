const mongoose = require('mongoose')
const Schema = mongoose.Schema

const playerSchema = new Schema(
	{
		_id: Number,
		age: Number,
		firstName: String,
		playerId: String,
		lastName: String,
		position: String,
		teamId: String,
		speedRating: Number,
		agilityRating: Number,
		accelRating: Number,
		throwAccShortRating: Number,
		throwAccMidRating: Number,
		throwAccDeepRating: Number,
		throwPowerRating: Number,
		throwOnRunRating: Number,
		playActionRating: Number,
		carryRating: Number,
		catchRating: Number,
		bCVRating: Number,
		elusiveRating: Number,
		routeRunRating: Number,
		truckRating: Number,
		stiffArmRating: Number,
		spinMoveRating: Number,
		jukeMoveRatin: Number,
		jumpRating: Number,
		cITRating: Number,
		releaseRating: Number,
		specCatchRating: Number,
		impactBlockRating: Number,
		runBlockRating: Number,
		passBlockRating: Number
	}, {
		usePushEach: true
	  }

)

module.exports = playerSchema
