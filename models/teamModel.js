const mongoose = require('mongoose')
const Schema = mongoose.Schema

const teamSchema = new Schema(
	{
		leagueId: String,
		teams: [{
			name: String,
			abbreviation: String,
			teamId: String,
			users: [
				{
					_id: false,
					username: String,
					firstSeen: [],
					lastSeen: []
				}
			]
		}]
	}

)

const teamModel = mongoose.model('team', teamSchema, 'teams')
module.exports = teamModel
